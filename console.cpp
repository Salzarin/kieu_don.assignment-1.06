#include "console.h"

console_t::console_t(){
  initConsole();
}



console_t::~console_t(){
  delwin(gameWind);
  delwin(info);
  delwin(debuginfo);
}

void console_t::print_info(std::string s){
  
  mvwprintw(info,0,0,s.c_str());
  
}


void console_t::initConsole(){
  debuginfo = newwin(1,MAX_H, 0, 0);
  info = newwin(2,MAX_H, 22, 0);
  gameWind = newwin(MAX_V,MAX_H, 1, 0);
}
WINDOW* console_t::getGameWindow(){
    return gameWind;
}


WINDOW* console_t::getInfoWindow(){
    return info;
}

WINDOW* console_t::getDebugWindow(){
    return debuginfo;
}


void console_t::refresh(){
  
    wrefresh(gameWind);
    wrefresh(info);
    wrefresh(debuginfo);
}

void console_t::clear(){
  
    wclear(gameWind);
    wclear(info);
    wclear(debuginfo);
}