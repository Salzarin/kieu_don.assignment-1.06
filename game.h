#ifndef GAME_H
#define GAME_H


#include <ncurses.h>
#include <cstdlib>
#include <chrono>
#include <algorithm>
#include <unistd.h>

#include "dungeon.h"
#include "player.h"
#include "monster.h"

class Flags{
  public:
  int _NumberMonsters;
  int _Max_Rooms;
  bool _save;
  bool _load;
  bool _player_auto;
  int _player_speed;
  std::string _filename;
  bool _verbose;
  Flags();
  ~Flags();
  Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms);
  Flags(const Flags& f);
};


class Game{
  


public:
  Game();
  Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms);
  ~Game();
  void gameLoop();
  
  
private:

  console_t* console;
  void gameInit();
  unsigned int getElapsedTime();
  Dungeon *d;
  player_t* p;
  bool _reveal;
  std::vector<character_t*> mList;
  std::chrono::time_point<std::chrono::system_clock> startTime;
  void initAllColors();
  Flags* flag;
  void generateMonsters();
  void deleteMonsters();
  void renderMonsters();
  void resetEventList();
  void GameOverScreen();
  void printMonsterList();
  void resetMap();
  
  std::vector<character_t*> eventQueue;

protected: 
  
  
};

#endif