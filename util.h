#ifndef UTILS_H	
#define UTILS_H

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <climits>
#include <cstring>


#define MAX_H 80
#define MAX_V 21
class position_t;
class dimension_t;

class position_t{
public:
  int _x;
  int _y;
  position_t operator+(const position_t& right);
  position_t operator-(const position_t& right);
  position_t operator+(const dimension_t& right);
  position_t operator/(const int& right);
  int operator*(const position_t& right);
  bool operator==(const position_t& right);
  
  position_t(int x,int y){
    _x = x;
    _y = y;
      
  }
  position_t(){
    _x = 0;
    _y = 0;
  }
  ~position_t(){
  }
  
  int x(){
  return _x;
  }
  int y(){
  return _y;
  }
  
};

class dimension_t{

public:
  int _w;
  int _h;
  position_t operator+(const dimension_t& right);
  position_t operator/(const int& right);
  
  dimension_t(int w, int h){
    _w = w;
    _h = h;
  }
  dimension_t(){
    _w = 0;
    _h = 0;
  }
  ~dimension_t(){
  }
  
  int w(){
  return _w;
  }
  int h(){
  return _h;
  }
  
};

typedef union _Endiandata{
  char str[4];
  int integer;
} endianData;


endianData endianSwap(endianData e);
bool checkEndian();

position_t getnewDirection(int d);
#endif