CXX = g++
CXXFLAGS =-g -std=c++11 -Wall
ODIR=obj

_OBJ = main.o dungeon.o game.o room.o util.o dijkstras.o character.o console.o player.o monster.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


all: add_dir | Dungeon

add_dir:
	mkdir -p $(ODIR)
	
$(ODIR)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $< -lncurses 
	
	
Dungeon: $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^  -lncurses

clean:	
	rm -f Dungeon *o *~ obj/*.o
	rmdir obj
tar:
	tar cvfz ./../kieu_don.assignment-1.06.tar.gz ./../kieu_don.assignment-1.06/ --exclude .git
changelog:
	git log --date=local --pretty=format:"%ad, %h, %an, message: %s" > CHANGELOG
