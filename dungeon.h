#ifndef DUNGEON_H
#define DUNGEON_H


#include <ncurses.h>
#include <cstring>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#define MAX_H 80
#define MAX_V 21

#include "console.h"
#include "room.h"
#include "util.h"
#include "dijkstras.h"
class Dungeon{

    
  private:
    int Max_Rooms;
    bool save;
    bool load;
    std::string filename;

        
    std::vector<Room> Rooms;
    
    void initDungeon();
    void drawBorders(bool mapExists);
    
    void initHardness();
    void createRooms();
    void drawRooms();
    void drawCorridors();
    void loadCorridors();
    void placeStairs(int type);
    void drawStairs();
    
    
    unsigned int dungeonMap[MAX_V][MAX_H];
    unsigned int hardnessMap[MAX_V][MAX_H];
    int parentTunnelMap[MAX_V][MAX_H];
    int parentNonTunnelMap[MAX_V][MAX_H];
    int playerDestinationMap[MAX_V][MAX_H];
    position_t upStairs;
    position_t downStairs;
    
  protected:
  
public:
    console_t* console;
    Dungeon();
    ~Dungeon();
    Dungeon(console_t* con, int MR, bool s, bool l, std::string fn);
    void renderDungeon();
    void init();
    int loadDungeon(std::string fname);
    int saveDungeon(std::string fname);
    std::vector<Room> getRooms();
    int checkHardness(position_t p);
    void createTunnelMap(position_t p);
    void createNonTunnelMap(position_t p);
    position_t getNext(position_t p,bool tunnel);
    void decreaseHardness(position_t p,unsigned int  amount);
    char checkMap(position_t p);
    int createDungeon();
    position_t getStairs(bool st);
    char getSym(position_t pos);
};



/*

enum direction{
  LEFT_DOWN,
  DOWN,
  RIGHT_DOWN,
  RIGHT,
  RIGHT_UP,
  UP,
  LEFT_UP,
  LEFT
};





typedef struct playerdata{
  int x;
  int y;
  int speed;
  int direction;
  bool _isAlive;
} player;

typedef struct stairs{
  int x;
  int y;
} stairs_t;



typedef union _data{
  char str[4];
  int integer;
} data;


typedef struct roomdata{
	//X and Y are position of top left of the room.
	int x; 
	int y;
	int w;
	int h;
	int cx;
	int cy;
	bool valid;
} room;


typedef struct dungeon{

  
  int dungeonmap[MAX_H][MAX_V];
  int rockhardnessmap[MAX_H][MAX_V]; //Hardness Map
  int TunnelMap[MAX_H][MAX_V]; //Tunnel to Player Map
  int NoneTunnelMap[MAX_H][MAX_V]; //Non-Tunneling to Player Map
  int PNTMap[MAX_H*MAX_V]; //Parental Map to Player with Non-Tunneling
  int PTMap[MAX_H*MAX_V];  //Parental Map to Player with Tunneling
  int PDestMap[MAX_H*MAX_V]; //Player Destination for Auto-Playing
  room Room[5000];
  int Max_Rooms;
  stairs_t upStairs;
  stairs_t downStairs;
  
} dungeon_t;



int getnewDirection(int d, int *x,int *y);
int getnewRandDirection(int d, int *x,int *y);

//dungeon.c
void initAllColors();
int translateKeyinput(int key);
bool validKeyCheck(int key);
bool checkRoomIntersection(room A, room B);
int createDungeon(dungeon_t *d);
int saveDungeon(char * filename, dungeon_t* d);
int loadDungeon(char * filename, dungeon_t* d);
data endianSwap(data e);
void initializeDungeon(dungeon_t *d, bool hardness_set);
void printDungeon(dungeon_t *d);
void drawRooms(dungeon_t *d);
void drawCorridors(dungeon_t *d);
void drawCorridorsHardness(dungeon_t *d);
void placeStairs(dungeon_t *d, int type);
void drawStairs(dungeon_t *d);
void printHardnessMap(int rockhardnessmap[MAX_H][MAX_V]);
void loadCorridors(dungeon_t *d);
void renderDungeon(player p, dungeon_t d,monster_list_t* mList);
void inCursesPrintDungeon(dungeon_t d);
int printMonsterList(player p, monster_list_t * monsters);
void gameoverMessage(player p, dungeon_t d,monster_list_t* mList);
void init_Events(player p,dungeon_t d,character events[5000], int* eventSize, int eventPos[5000], monster_list_t * mList, monster_node_t ** monsters, int* nummon, struct timespec ti);
void cleanEvents(character events[5000], int* eventSize, int eventPos[5000], monster_list_t * mList, monster_node_t ** monsters);

//Monster.c 
void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V], int PTMap[MAX_H*MAX_V]);
void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V],int PNTMap[MAX_H*MAX_V]);
void initMonster(player p, dungeon_t d, monster_list_t * e, monster_node_t ** monsters, int *nummon);
void printMonsters(monster_list_t *monsters);
void monsterMovement(player p, monster *mon, int PNTmap[MAX_H*MAX_V], int PTmap[MAX_H*MAX_V], int Hmap[MAX_H][MAX_V], int * new_x, int *new_y);
bool checkLos(monster *mon, player p, dungeon_t d);



void printNoTunnelMap(player p,int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V]);
void printTunnelMap(player p, int Tmap[MAX_H][MAX_V]);
void printColor(int i);

//Player.c
int movePlayer(player *p, int dx, int dy,int hardmap[MAX_H][MAX_V],int dungeonmap[MAX_H][MAX_V]);
void printPlayer(player p);
void setPlayerPosition(player *p, dungeon_t d, bool player_set,int Ascending);
*/



#endif