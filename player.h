#ifndef PLAYER_H
#define PLAYER_H
#include "character.h"
#include "dungeon.h"
#include "util.h"

class player_t : public character_t{
public:
   player_t();
   ~player_t();
   player_t(console_t* con, Dungeon * dungeon, int spd);
   void setRandomPos();
   void teleportMode(std::vector<character_t*> mList);
   int movePlayer();
   int playerCommand(int key);
   int translateKeyinput(int key);
   bool validKeyCheck(int key);
   char seenMap[MAX_V][MAX_H];
   void renderMap(std::vector<character_t*> mList);
   bool checkLos(position_t check);
   void resetMap();
private:
  Dungeon* d;
protected:
 
};



#endif