#include "util.h"



position_t position_t::operator+(const position_t& right){
  position_t temp;
  temp._x = _x+right._x;
  temp._y = _y+right._y;
  return temp;  
}

position_t position_t::operator+(const dimension_t& right){
  position_t temp;
  temp._x = _x+right._w;
  temp._y = _y+right._h;
  return temp;  
}
position_t position_t::operator-(const position_t& right){
  position_t temp;
  temp._x = _x-right._x;
  temp._y = _y-right._y;
  return temp;  
}

position_t position_t::operator/(const int &s){
  position_t temp;
  temp._x = _x/s;
  temp._y = _y/s;
  return temp;  
}

bool position_t::operator==(const position_t& right){

  return (_x == right._x && _y == right._y);
}


position_t dimension_t::operator+(const dimension_t& right){
  position_t temp;
  temp._x = _w+right._w;
  temp._y = _h+right._h;
  return temp;  
}

position_t dimension_t::operator/(const int &s){
  position_t temp;
  temp._x = _w/s;
  temp._y = _h/s;
  return temp;  
}

int position_t::operator*(const position_t& right){
  return abs(_x*right._x)+abs(_y*right._y);
}


endianData endianSwap(endianData e){
  endianData final;
  final.str[0] = e.str[3];
  final.str[1] = e.str[2];
  final.str[2] = e.str[1];
  final.str[3] = e.str[0];
  return final;
}

bool checkEndian(){
  int i = 1;
  
  return (*(char *)&i == 1);
  
}


position_t getnewDirection(int d){
  int _DX[8]={-1,0,1,1, 1, 0,-1,-1};  
  int _DY[8]={ 1,1,1,0,-1,-1,-1, 0};
  position_t pos = position_t(0,0);
  if(d<8)
  pos = position_t(_DX[d],_DY[d]);
  return pos;
}











