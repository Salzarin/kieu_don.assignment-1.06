#ifndef MONSTER_H
#define MONSTER_H

#include "character.h"
#include "player.h"
#include "util.h"
#include<vector>
#include <ncurses.h>

class monster_t : public character_t{
public:
  monster_t();
  ~monster_t();
  monster_t(console_t* con,player_t* p,  Dungeon * dungeon, int spd);
  bool initMonsterPosition(bool mMap[MAX_V][MAX_H]);
  void initMonsterAttr();
  void setRandomPos();
  player_t * player;
  position_t dest;
  int attr;
  void monsterNextMove();
  void moveMonster();
  bool checkLos();
  bool seen;
private:
  Dungeon* d;
  
};



#endif