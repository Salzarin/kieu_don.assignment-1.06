#ifndef CONSOLE_H
#define CONSOLE_H
#include "ncurses.h"
#include <string>
#include "util.h"
class console_t{
  WINDOW * debuginfo;
  WINDOW * gameWind;
  WINDOW * info;
  void initConsole();
  
  

public:
  console_t();
  ~console_t();
  WINDOW* getGameWindow();
  WINDOW* getInfoWindow();
  WINDOW* getDebugWindow();
  void print_info(std::string s);
  void refresh();
  void clear();
  
};






#endif