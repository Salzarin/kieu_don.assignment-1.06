#include "heap.h"
#include "dijkstras.h"



class vertex{
public:
  int Vertex;
  int Weight;
  int Parent;
  int ID;
  
  vertex(int id, int v,int w,int p){
    ID = id;
    Vertex = v;
    Weight = w;
    Parent = p;
  }
  vertex(){
    ID = 0;
    Vertex = -1;
    Weight = INT_MAX;
    Parent = -1;
  }
    bool operator()(vertex* a,vertex* b,bool swap){
      
	if(swap){
	  vertex temp = *b;
	  (*b).ID = (*a).ID;
	  (*a).ID = temp.ID;
	}
      return (*a).Weight<(*b).Weight;
    }
    
    int operator()(vertex* a){ 
      return (*a).ID;
    }
};



void dijkstras(unsigned int map[MAX_V][MAX_H], int start, int end, bool monster, bool tunneler, int PMap[MAX_V][MAX_H]){


int i;
int total_vertex = MAX_H*MAX_V;
std::vector<int> path;
typedef std::vector<position_t> edge;

std::vector<edge> edgeList;
//Set our Node List to Null

for(i=0;i<total_vertex;i++){
  edge temp;
  edgeList.push_back(temp);
}

//Traverse Through Hardness Map And Add Edges
//Basically building our network of nodes.


int index = 0;
for(index=0;index<total_vertex;index++){
  
  
  //int index = i+j*MAX_H;
  int cells[8];
  cells[0] = index-MAX_H;//Up
  cells[1] = index+1;//Right
  cells[2] = index+MAX_H;//Down
  cells[3] = index-1;//Left

  cells[4] = index-MAX_H-1;//Up-Left
  cells[5] = index-MAX_H+1;//Up-Right
  cells[6] = index+MAX_H+1;//Down-Right
  cells[7] = index+MAX_H-1;//Down-Left
  
  
  int Max_Cells = 4;
  
  if(monster){
    Max_Cells = 8;
  }
  int n;
  edge t;
  for(n = 0;n<Max_Cells;n++){
    if(cells[n]>= 0 && cells[n]<(MAX_H*MAX_V)){
      int x = cells[n]%MAX_H;
      int y = cells[n]/MAX_H;
      if(x>0 && x<MAX_H && y>0 && y<MAX_V){
	int hard = map[y][x];
	position_t value = position_t(cells[n],hard);
	
	if(map[y][x] != 255){ //As Long as it's not a wall

	  if(monster){ //If it is a monster
	    if(tunneler){
	      if(hard+1 != 255){
	      hard = hard==0 ? 1 : 1+(hard)/85;
	      value = position_t(cells[n],hard);
	      t.push_back(value);
		
	      }
	    }
	    else{
	      if(hard == 0){
		value = position_t(cells[n],hard);
		t.push_back(value);
	      }					
	    }
	  }
	  else{ //If it is cooridor
	    t.push_back(value);
	  }
	}
      }
    }			
  }
  
  edgeList.at(index) = t;
}


vertex min;
minHeap<vertex> heap;

//Initial all routes.
for(i = 0; i<total_vertex;i++){
	heap.push(vertex(i,i,INT_MAX,-1));
	PMap[i/MAX_H][i%MAX_H] = -1;
}

//Set Distance of Source to zero

(*(heap.list.at(start))).Weight = 0;
PMap[start/MAX_H][start%MAX_H] = start;
heap.buildHeap();
while(heap.heap.size()){

	min = heap.pop(); 
	edge node= edgeList.at(min.Vertex);
	edge::iterator etr;
	
	if(start!=end && min.Vertex == end){
	  break;
	}
	
	for(etr = node.begin(); etr!=node.end();etr++){
		int check = (*etr).x();
		int u = min.Weight;
		int v = heap.peekHeap(check).Weight;
		int w = (*etr).y();
		
		if(u != INT_MAX && (v > (u+w))){
			
			v = u+w;
			PMap[check/MAX_H][check%MAX_H] = min.Vertex;
			vertex change = heap.peekHeap(check);
			change.Weight = v;
			heap.decreaseKey(check,change);
		}
		
			
	}
	
}

}