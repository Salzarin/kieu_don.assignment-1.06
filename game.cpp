#include "game.h"



Flags::Flags(){
  
  _NumberMonsters = 5;
  _Max_Rooms = 5;
  _save = false;
  _load = false;
  _player_auto = false;
  _player_speed = 10;
  std::string _filename = "";
}

Flags::~Flags(){
  
  
}

Flags::Flags(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms){
  _NumberMonsters = nummon;
  _Max_Rooms = max_rooms;
  _save = s;
  _load = l;
  _player_auto = a;
  _player_speed = ps;
  _verbose = v;
  _filename = fn;
}

Flags::Flags(const Flags& f){
    _NumberMonsters = f._NumberMonsters;
  _Max_Rooms = f._Max_Rooms;
  _save = f._save;
  _load = f._load;
  _player_auto = f._player_auto;
  _player_speed = f._player_speed;
  _verbose = f._verbose;
  _filename = f._filename;
}

Game::Game(std::string &fn,bool v, bool l, bool s, bool a, int ps, int nummon, int max_rooms) : Game(){
  flag = new Flags(fn,v,l,s,a,ps,nummon,max_rooms);
  gameInit();
}


Game::Game(){
  
  initscr();
  console = new console_t();
  raw();
  noecho();
  curs_set(0);
  start_color();
  initAllColors();
  _reveal = false;
}

Game::~Game(){
  delete d;
  delete p;
  delete console;
  delete flag;
  deleteMonsters();
  endwin();
}

unsigned int Game::getElapsedTime(){  
  return std::chrono::duration_cast<std::chrono::seconds>
  (std::chrono::system_clock::now() - startTime).count();
}

void Game::deleteMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    delete (*itr);
    (*itr)=NULL;
  }
  mList.clear();
}
void Game::generateMonsters(){
  bool monsterMap[MAX_V][MAX_H];
  memset(monsterMap,false,sizeof(monsterMap));
  deleteMonsters();
  int i;
  for(i = 0; i<flag->_NumberMonsters;i++){ 
    mList.push_back(new monster_t(console, p, d, rand()%15+5));
    if(((monster_t *)mList.back())->initMonsterPosition(monsterMap)){
      eventQueue.push_back(mList.back());
      monsterMap[mList.back()->getPos().y()][mList.back()->getPos().x()]=true;
    }
    else{
      delete mList.back();
      mList.back() =NULL;
      mList.erase(mList.end()-1);
      break;
    }
  }
  
}

void Game::renderMonsters(){
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
    (*itr)->renderCharacter();
  } 
}

void Game::resetEventList(){
  eventQueue.clear();
  std::vector<character_t*>::iterator itr;
  for(itr = mList.begin();itr!=mList.end();itr++){
   eventQueue.push_back((*itr));
  }
  
  
  
}

void Game::gameInit(){
  startTime = std::chrono::system_clock::now();
  d = new Dungeon(console,flag->_Max_Rooms, flag->_save, flag->_load, flag->_filename);
  p = new player_t(console, d, flag->_player_speed);
  p->setEventTime(0);
  eventQueue.push_back(p);
  generateMonsters();
}


void Game::printMonsterList(){
  int size = 0;
  int key = 0;
  int i;
  int start = 0;
  int w = MAX_H*3/4;
  int h = MAX_V*3/4;
   
  char lat[][6] = {"North", "", "South"};
  char lon[][6] = {"West", "", "East"};
  int list_size = h;
  
  WINDOW* monsterList = newwin(h,w,(MAX_V-h)/2,(MAX_H-w)/2);
  
  keypad(monsterList,TRUE);

  size = mList.size();
  set_escdelay(0);
  while(key!=27 && key!='Q'){
    wclear(monsterList);
    std::vector<character_t*>::iterator itr=mList.begin();
    for(i=0; i<start;i++){
      itr++;
    }
    for(i=start; i<(start+list_size) && itr<mList.end();i++){
	wprintw(monsterList, "%d Monster %c ", i, (*itr)->getSym());
	position_t dp = ((*itr)->getPos())-(p->getPos());
	if((dp.x()<0?-1:dp.x()>0)!=0){
	wprintw(monsterList, "%d %s",abs(dp.x()),lon[(dp.x()<0?-1:dp.x()>0) +1]);
	  if((dp.y()<0?-1:dp.y()>0)!=0){
	    wprintw(monsterList, ", ");
	  }
	}
	if((dp.y()<0?-1:dp.y()>0)!=0){
	  wprintw(monsterList, "%d %s\n",abs(dp.y()),lat[(dp.y()<0?-1:dp.y()>0)+1]);
	}
	else{
	  wprintw(monsterList,"\n");  
	}
	itr++;
    }
    
    key = wgetch(monsterList);
    printw("%d", key);
    wrefresh(monsterList);
    
    if(key == KEY_UP){
      
      if(start>0){
	start--;
      }
    }
    
    if(key == KEY_DOWN){
      if((start+list_size)<size){
	start++;
      }
    }
    
  }
  delwin(monsterList);
  
  
}

void Game::resetMap(){
  
  d->createDungeon();
  generateMonsters();
  resetEventList();
  
}


void Game::gameLoop(){
  int key = 0;
  int count = 0;
  
  
    if(_reveal){
      d->renderDungeon();
      renderMonsters();
    }
    else{
      p->renderMap(mList);
    }
  p->renderCharacter();
  
  while(key!=27 && key!='Q' && p->getAlive() && mList.size()){
  
    
    //Clear Everything in Console Before Rendering
    
    std::make_heap(eventQueue.begin(),eventQueue.end(),character_t());
    std::sort_heap(eventQueue.begin(),eventQueue.end(),character_t());
    //Get Next Character
    character_t* character = eventQueue.front();
    std::pop_heap(eventQueue.begin(),eventQueue.end());
    eventQueue.pop_back();
    
    if(!character){
      
      continue;
    }
    
    
    std::vector<character_t *>::iterator itr;
    for(itr=eventQueue.begin();itr!=eventQueue.end();itr++){
      (*itr)->decreaseEventTime(character->getEventTime());
    }
    
    
    
    key = 0;
    int waitTime = character->getEventTime(); 
    
    
    if(character->getSym() == '@'){
      //Handle Player Behaviour

      p->setEventTime();
      key = p->playerCommand(key);
      
      if(key == '<' || key == '>'){
	//Reset Map
	if(d->checkMap(p->getPos()) == '<' && key=='<'){
	  resetMap();
	  p->setPos(d->getStairs(1));
	  p->resetMap();
	}
	if(d->checkMap(p->getPos()) == '>'&& key=='>'){
	  resetMap();
	  p->setPos(d->getStairs(0));
	  p->resetMap();
	}
	p->setEventTime(0);
      }
      
      if(key == 'm'){
	//Render Monster List
	printMonsterList();
	p->setEventTime(0);
      }
      
      if(key == 't'){
	//Teleport Mode
	p->teleportMode(mList);
	p->setEventTime(0);
      }
      if(key == 'f'){
	_reveal = !_reveal;
	p->setEventTime(0);
      }
      //Check if monster next space
      std::vector<character_t*>::iterator mitr, at;
      for(mitr=mList.begin();mitr!=mList.end();mitr++){
	if((*mitr)->getPos() == p->npos){
	  //(*mitr)->setAlive(false);
	  delete (*mitr);
	  (*mitr) = NULL;
	  mList.erase(mitr);
	  //Clear Event Queue
	  resetEventList();
	  break;
	  
	}
      }

      //Move the Player
      p->movePlayer();

      wprintw(console->getDebugWindow(),"%d",p->getEventTime());
      wrefresh(console->getDebugWindow());
      eventQueue.push_back(p);
      std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
      
    }
    else{
      
	monster_t* m = dynamic_cast<monster_t*>(character);
	if(m){
	  wprintw(console->getInfoWindow(),"Render %c : %d",m->getSym(), m->getSpeed());
	  wrefresh(console->getInfoWindow());
	  
	  m->monsterNextMove();
	  //Check If Player
	  if(m->npos== p->getPos()){
	    p->setAlive(false);
	  }
	  
	  m->moveMonster();
	  
	  //key = wgetch(console->getGameWindow());
	  m->setEventTime();
	  eventQueue.push_back(m);
	  std::push_heap(eventQueue.begin(),eventQueue.end(),character_t());
	}
    }

    console->clear();
    
    //Render All Console;
    if(_reveal){
    d->renderDungeon();
    renderMonsters();
    }
    else{
    p->renderMap(mList);
    }
    p->renderCharacter();
    count++;

    console->refresh();
      if(waitTime> 0){ 
	usleep(waitTime*10);
      }
  }
  GameOverScreen();
}



void Game::GameOverScreen(){
    console->clear();
    d->renderDungeon();
    renderMonsters();
    p->renderCharacter();
    
    if(mList.size()){
      mvwprintw(console->getInfoWindow(),0,0,"You Died: Monsters Left %d",mList.size());
    }
    else{
      mvwprintw(console->getInfoWindow(),0,0,"You Win.");
    }
      mvwprintw(console->getInfoWindow(),1,0,"Press Q to quit.");
     
    console->refresh();
    
    while(wgetch(console->getGameWindow())!='Q');
}


void Game::initAllColors(){
  init_pair(1, COLOR_BLACK, COLOR_YELLOW);//Los background
  init_pair(18, COLOR_BLACK, COLOR_BLUE);//Fog background
  
  init_pair(2, COLOR_RED,COLOR_BLACK); //Erratic
  init_pair(3, COLOR_YELLOW,COLOR_BLACK); 
  init_pair(4, COLOR_GREEN,COLOR_BLACK);
  init_pair(5, COLOR_BLUE,COLOR_BLACK); //Intelligent
  init_pair(6, COLOR_CYAN,COLOR_BLACK);
  init_pair(7, COLOR_MAGENTA,COLOR_BLACK);

  init_pair(12, COLOR_RED,COLOR_YELLOW); //Erratic
  init_pair(13, COLOR_YELLOW,COLOR_YELLOW); 
  init_pair(14, COLOR_GREEN,COLOR_YELLOW);
  init_pair(15, COLOR_BLUE,COLOR_YELLOW); //Intelligent
  init_pair(16, COLOR_CYAN,COLOR_YELLOW);
  init_pair(17, COLOR_MAGENTA,COLOR_YELLOW);
  init_pair(8, COLOR_WHITE,COLOR_RED);  
}